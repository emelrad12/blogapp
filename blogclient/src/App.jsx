import React, { Component } from 'react';
import PostSmall from './components/posts/PostSmall'
import styled from 'styled-components'
import CreatePost from './components/posts/CreatePost'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { getAllPosts } from './components/graphql/queries/posts'


class App extends Component {
    state = { posts: [] }
    async componentWillMount() {
        let posts = await getAllPosts();
        this.setState({ posts: posts })
    }

    render() {
        return (
            <Posts>
                <Router>
                    <Link to="/">Home</Link>
                    <Link to="/CreatePost">Create Post</Link>
                    <Route exact path="/" component={() => this.listPosts(this.state.posts)} />
                    <Route path="/CreatePost" exact component={CreatePost} />
                </Router>
            </Posts>
        )
    }

    listPosts(posts) {
        if (posts) {
            console.log("sass",posts)
            return posts.map((post) => {
                return <PostSmall post={post} />
            })
        }
    }
}
export default App;

const Posts = styled.div`
margin:0 auto 0 auto;
max-width:500px;

`