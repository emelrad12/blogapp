import client from '../ApolloClient'
import { gql } from "apollo-boost";
import { getCurrentUser } from '../../services/userService';
export async function getAllPosts() {
  let result = [];
  await client.query({
    query: gql`{
      Post{
        author
        date
        location
        linkToPost
        content
        title
      }
  }`
  }).then((posts) => { result = posts.data.Post });
  return result;
}
export async function addPost(postIn, content, title) {
  getCurrentUser();
  let result = [];
  await client.query({
    mutation: gql`{
        AddPost(
        author: ${author}
        date: ${date}
        location: ${location}
        linkToPost: ${linkToPost}
        content: ${content}
        title: ${title}
  )
}
  }`
  }).then((posts) => { result = posts.data.Post });
  return result;
}




