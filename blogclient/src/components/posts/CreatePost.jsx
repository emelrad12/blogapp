import React, { Component } from 'react';
import styled from 'styled-components'
import { Form, Field } from 'react-final-form'
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import draftToHtml from 'draftjs-to-html';
import { addPost } from '../graphql/queries/posts';

const required = value => (value ? undefined : "Required");

  
class CreatePost extends Component {
    state ={
        markup:String
    }
    ConvertToHtml=(draft) =>{
        const markup = draftToHtml(draft);
        this.setState({markup:markup})
    }
    onSubmit=(data)=>{
        addPost(...data)
        data.content=this.state.markup;
        console.log(data)
    }
    render() {
        return (
            <FormContainer>
                <Form
                    onSubmit={this.onSubmit}
                    render={({ handleSubmit, pristine, invalid }) => (
                        <form onSubmit={handleSubmit}>
                            <h2>Create Post</h2>
                            <div>
                                <label>Title: </label>
                                <Field name="Title" component="input" placeholder="Title" validate={required} />
                            </div>
                            <div>
                                <label>PostIn: </label>
                                <Field name="PostIn" component="input" placeholder="PostIn" validate={required} />
                            </div>
                            <div>
                                <Editor
                                    toolbarClassName="toolbarClassName"
                                    wrapperClassName="wrapperClassName"
                                    editorClassName="editorClassName"
                                    onEditorStateChange={this.onEditorStateChange}
                                    onChange={this.ConvertToHtml}
                                />
                            </div>
                            <button type="submit" disabled={pristine || invalid}>
                                Submit
                            </button>
                        </form>
                    )}
                />
            </FormContainer>
        )
    }
}
export default CreatePost

const FormContainer = styled.div``