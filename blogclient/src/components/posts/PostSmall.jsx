import React, { Component } from 'react';
import styled from 'styled-components'
class PostSmall extends Component {
    listContent(cont) {
        return cont.map((c) => {
            return <Entry dangerouslySetInnerHTML={{ __html: c }} />
        })
    }
    render() {
        return (
            <PostContainer>
                <Title href="https://google.com">
                    {this.props.post.title}
                </Title>
                <Posted>
                   Posted by {this.props.post.author} at {this.props.post.date} in <a href={this.props.post.linkToPost}>{this.props.post.location}</a>
                </Posted>
                <Content dangerouslySetInnerHTML={{ __html: this.props.post.content }}/>
            </PostContainer>
        )
    }
}
export default PostSmall

const PostContainer = styled.div`
margin:20px;
`

const Title = styled.a`
font:verdana;
font-weight: bold;
text-decoration: underline;

font-size:20px;
color:#1E88FE;
:hover{
    text-decoration: none;
    color:#31B9FF;
}
`

const Posted = styled.div`
margin:5px 0 10px 0;
`

const Content = styled.div`
`
const Entry = styled.div`
`