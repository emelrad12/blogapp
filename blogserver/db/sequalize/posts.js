const { Posts } = require('./models/posts.js');

export class PostsSeq {
  async getAll() {
    return await Posts.findAll().then(result => {
      return result;
    });
  }
  async getById(id, fields) {
    return await Posts.findByPk(id, { attributes: fields }).then(result => {
      return [result];
    });
  }
  async add(author, date, location, linkToPost, content, title) {
    console.log(date)
    await Posts.build({ author: author, date: date, location: location, linkToPost: linkToPost, content: content, title: title }).save();
    return true;
  }
}