import { Model, DataTypes } from "sequelize";
import { sequelize } from "../main.js";
export const Posts = sequelize.define('Posts', {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true
  },
  content: {
    type: DataTypes.STRING
  },
  author: {
    type: DataTypes.STRING
  },
  date: {
    type: DataTypes.DATE
  },
  title: {
    type: DataTypes.STRING
  },
  linkToPost: {
    type: DataTypes.STRING
  },
  location: {
    type: DataTypes.STRING
  }
}, {
    timestamps: false
  });




/* export class Posts extends Model {
  static init() {
    super.init({
      id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
      },
      content: {
        type: DataTypes.STRING
      },
      postesrName: {
        type: DataTypes.STRING
      },
      date: {
        type: DataTypes.DATE
      },
      title: {
        type: DataTypes.STRING
      }
    },
      {
        sequelize,
        timestamps: false
      })
  }
}; */