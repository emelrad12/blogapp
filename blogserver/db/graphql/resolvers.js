import { PostsSeq } from '../sequalize/posts.js'
const resolvers = {

    Query: {
        Post(parent, args, ctx, info) {
            let fields = (info.fieldNodes[0].selectionSet.selections.reduce((acc, cur) => {
                acc.push(cur.name.value);
                return acc;
            }, [])
            )
            if (args.id) {
                return new PostsSeq().getById(args.id, fields);
            }
            return new PostsSeq().getAll();
        }
    },
    Mutation: {
        async AddPost(parent, args, ctx, info) {
            //console.log(args )
            return new PostsSeq().add(args.author, args.date, args.location, args.linkToPost, args.content, args.title);
        }
    }
}
export default resolvers

