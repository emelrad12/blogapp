const { gql } = require('apollo-server');
const typeDefs = gql`
    type Post{
        author: String,
        date: String,
        location:String,
        linkToPost:String,
        content: String,
        title:String,
        id: Int,
    }
    type Query {
        Post(id:Int):[Post]!,
    }
    type Mutation {
        AddPost(author: String!,date: String, location: String!, linkToPost:String!, content:String!, title:String!):String
    }
`
export default typeDefs