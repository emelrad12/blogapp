import express from 'express';
import cors from 'cors';
import { PostsSeq } from './db/sequalize/posts.js'
import bodyParser from 'body-parser';
import { ApolloServer, gql } from 'apollo-server';
import resolvers from './db/graphql/resolvers.js'
import typeDefs from './db/graphql/schema.js'
const app = express();
const port = 1500;
const server = new ApolloServer({ typeDefs, resolvers });
server.listen().then(({ url }) => {
    console.log(`🚀  Server ready at ${url}`);
});

app.use(bodyParser());
app.use(cors());
app.get('/', (req, res) => sendResponse(req, res));
app.listen(port);
async function sendResponse(req, res) {
    var post = await new PostsSeq().getAll();
    res.send(post);
    res.end();
}